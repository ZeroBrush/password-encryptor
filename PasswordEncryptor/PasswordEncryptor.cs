﻿using PasswordEncryptor;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace PasswordService
{
    internal class PasswordEncryptor : IPasswordEncryptor
    {
        private CryptoSetting setting;

        public PasswordEncryptor(CryptoSetting setting)
        {
            this.setting = setting;
        }

        private byte[] GenerateSalt()
        {
            return RandomNumberGenerator.GetBytes(setting.SaltSize);
        }

        private CryptoSetting GetSetting()
        {
            return setting;
        }

        private byte[] GenerateSaltedPassword(byte[] password, byte[] salt, CryptoSetting setting)
        {
            byte[] concatenatedSalt = salt.Concat(password).ToArray();

            return setting.HashAlgorithmType.ComputeHash(concatenatedSalt);
        }

        private static byte[] HashPassword(byte[] password, byte[] salt, int iterations)
        {
            using (var rfc2898 = new Rfc2898DeriveBytes(password, salt, iterations))
            {
                return rfc2898. GetBytes(32);
            }
        }

        public CryptingResult EncryptPassword(String password)
        {
            byte[] salt = GenerateSalt();
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] saltedPassword = GenerateSaltedPassword(passwordBytes, salt, GetSetting());
            byte[] encryptedPassword = HashPassword(saltedPassword, salt, this.setting.IterantionCount);

            return new CryptingResult(encryptedPassword, salt, this.setting);
        }

        public CryptingResult EncryptPassword(String password, byte[] salt, int iterationCount)
        {
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] saltedPassword = GenerateSaltedPassword(passwordBytes, salt, GetSetting());
            byte[] encryptedPassword = HashPassword(saltedPassword, salt, iterationCount);

            return new CryptingResult(encryptedPassword, salt, this.setting);
        }
    }
}
