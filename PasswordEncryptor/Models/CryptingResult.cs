﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordService
{
    internal class CryptingResult
    {
        private byte[] encryptedPassword;
        public byte[] EncryptedPassword { get { return encryptedPassword; } set { encryptedPassword = value; } }

        private byte[] salt;
        public byte[] Salt { get { return salt; } set { salt = value; } }

        private CryptoSetting passwordVersion;
        public CryptoSetting PasswordVersion { get { return passwordVersion; } set { passwordVersion = value; } }

        public CryptingResult(byte[] encryptedPassword, byte[] salt, CryptoSetting passwordVersion)
        {
            this.encryptedPassword = encryptedPassword;
            this.salt = salt;
            this.passwordVersion = passwordVersion;
        }
    }
}
