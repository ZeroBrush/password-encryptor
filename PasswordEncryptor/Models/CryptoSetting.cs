﻿using System;
using System.Security.Cryptography;

namespace PasswordService
{
    public class CryptoSetting
    {
        private string ?version;

        public string ?Version { get { return version; } set { version = value; } }

        private int saltSize;

        public int SaltSize { get { return saltSize; } set { saltSize = value; } }

        private int iterantionCount;

        public int IterantionCount { get { return iterantionCount; } set { iterantionCount = value; } }

        private HashAlgorithm hashAlgorithmType;

        public HashAlgorithm HashAlgorithmType { get { return hashAlgorithmType; } set { hashAlgorithmType = value; } }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public CryptoSetting (string version, int saltSize, int iterationCount, string hashType)
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        {
            if(saltSize == 0 || iterantionCount == 0 || string.IsNullOrEmpty(hashType))
            {
                throw new ArgumentNullException("SaltSize, IterationCount, HashType must have valid values");
            }

            this.version = version;
            this.SaltSize = saltSize;
            this.IterantionCount = iterationCount;
#pragma warning disable CS8601 // Possible null reference assignment.
            this.HashAlgorithmType = HashAlgorithm.Create(hashType);
#pragma warning restore CS8601 // Possible null reference assignment.
        }
        
    }
}