﻿using PasswordService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordEncryptor
{
    internal interface IPasswordEncryptor
    {
        CryptingResult EncryptPassword(string password);

        CryptingResult EncryptPassword(string password, byte[] salt, int iterationsCount);
    }
}
